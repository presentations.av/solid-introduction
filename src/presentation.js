// Import React
import React from 'react';

import {
  LiveProvider,
  LiveEditor,
  LiveError,
  LivePreview
} from 'react-live'

import {Value, Name, List as SolidList, Image as SolidImage, Link as SolidLink} from '@solid/react';

// Import Spectacle Core tags
import {
  BlockQuote,
  Cite,
  Deck,
  Heading,
  ListItem,
  List,
  Quote,
  Slide,
  Text,
  Image,
  Appear,
  Link,
  CodePane,
} from 'spectacle';

// Import theme
import createTheme from 'spectacle/lib/themes/default';

import solidserver from './images/solid-server.png'
import markbook from './images/markbook.png'
import pixolid from './images/pixolid.png'

// Require CSS
require('normalize.css');


const theme = createTheme(
  {
    primary: 'white',
    secondary: '#1F2022',
    tertiary: '#7C4DFF',
    quaternary: '#18A9E6',
    source: '#666'
  },
  {
    primary: {
      name: 'Roboto',
      googleFont: true,
      styles: ['400', '700i']
    },
    secondary: 'Helvetica',
  }
);

const ruben = "https://ruben.verborgh.org/profile/#me";
const ccBy40 = {
  id: "https://creativecommons.org/licenses/by/4.0/",
  title: "Creative Commons Attribution 4.0."
};

const Source = ({webId, license}) => <div>
  <Text textColor="source" textSize={14}>by <Link textColor="source" href={webId}><Name src={webId} /></Link>, <Link textColor="source"  href={license.id}>{license.title}</Link></Text>
</div>;

const ByRuben = () => <Source webId={ruben} license={ccBy40}/>;

const URL = ({href, small, ...props}) => <Link textSize={small ? 20 : undefined} {...props} href={href}>{href}</Link>;

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck
        transition={['zoom', 'slide']}
        transitionDuration={500}
        theme={theme}
      >
        <Slide>
          <Heading size={1} fit caps lineHeight={1} textColor="secondary">
            Social Linked Data
          </Heading>
          <Text margin="10px 0 0" textColor="tertiary" size={1} fit bold>
            An introduction to the Solid project.
          </Text>
        </Slide>
        <Slide>
          <div style={{display: 'flex'}}>
            <SolidImage style={{borderRadius: 50}} src="[https://angelo.veltens.org/profile/card#me].image"/>

            <div>
              <Heading textColor="tertiary" size={4}><Value src="[https://angelo.veltens.org/profile/card#me].name"/></Heading>
              <Heading size={6}><Value
                  src="[https://angelo.veltens.org/profile/card#me].vcard_role"/></Heading>
              <Link href="https://angelo.veltens.org/profile/card#me" textSize={20} style={{marginTop: '1rem'}}>https://angelo.veltens.org/profile/card#me</Link>

            </div>
          </div>
        </Slide>
        <Slide bgColor="secondary">
          <BlockQuote>
            <Quote>I’ve always believed the web is for everyone. That’s why I
              and others fight fiercely to protect it</Quote>
            <Cite textColor="quaternary">Tim Berners-Lee</Cite>
          </BlockQuote>
        </Slide>
        <Slide>
          <List>
            <Heading textColor="tertiary" lineHeight={1} caps>
              Social Web?
            </Heading>
            <Heading size={2} lineHeight={1} caps>
              Social?
            </Heading>
            <Heading size={2} lineHeight={1} caps>
              Web?
            </Heading>
          </List>
        </Slide>
        <Slide>
          <List>
            <Heading size={6} textColor="tertiary">
              What do we expect of a social web?
            </Heading>
            <Text>Interact with others <span role="img" aria-label="thumb up">👍</span></Text>
            <Text>Share things we love ❤</Text>
            <Text>Express ourselves freely <span role="img" aria-label="speech bubble">💬</span></Text>
          </List>
        </Slide>
        <Slide>
          <List>
            <Heading size={6} textColor="tertiary">
              What do we have today?
            </Heading>
            <Text>Interactions limited to platform</Text>
            <Text>No interoperable data</Text>
            <Text>Zero privacy</Text>
          </List>
        </Slide>
        <Slide>
          <List>
            <Text>The WWW is a web of interlinked documents.</Text>
            <Appear><Text textColor="tertiary">Solid is a web of interlinked,
              structured, social data.</Text></Appear>
            <Appear><Text>Solid is based on the WWW and extends
              it.</Text></Appear>
          </List>
        </Slide>
        <Slide>
          <Heading size={6} margin="0 0 2rem 0" textColor="tertiary">
            All user data is stored to one or more Solid pods
          </Heading>
          <Image width="100%" src={"https://rubenverborgh.github.io/Web-Foundation-2018/images/decentralization-axis.svg"}/>
          <ByRuben/>
        </Slide>
        <Slide>
          <Heading size={6} margin="0 0 2rem 0" textColor="tertiary">
            Apps aggregate data from the distributed Solid Web
          </Heading>
          <Image width="100%" src={"https://rubenverborgh.github.io/Web-Foundation-2018/images/decentralized-data.svg"}/>
          <ByRuben/>
        </Slide>
        <Slide>
          <Heading size={6} margin="0 0 2rem 0" textColor="tertiary">
            Apps act as different views on the same data
          </Heading>
          <Image width="100%" src={"https://rubenverborgh.github.io/Web-Foundation-2018/images/apps-as-views.svg"}/>
          <ByRuben/>
        </Slide>
        <Slide>
          <Heading size={6} margin="0 0 2rem 0" textColor="tertiary">
            Separte markets for apps and storage providers
          </Heading>
          <Image width="100%" src={"https://rubenverborgh.github.io/Web-Foundation-2018/images/data-and-app-markets.svg"}/>
          <ByRuben/>
        </Slide>
        <Slide>
          <Heading>So, what <em>is</em> Solid?</Heading>
          <List>
            <Appear><ListItem>A set of web standards</ListItem></Appear>
            <Appear><ListItem>An ecosystem of compatible tools, libs and apps</ListItem></Appear>
            <Appear><ListItem>A community with a vision</ListItem></Appear>
          </List>
        </Slide>
        <Slide>
          <Heading>Permissionless innovation</Heading>
          <List>
            <Appear><ListItem>Store any kind of data on solid pods</ListItem></Appear>
            <Appear><ListItem>Build any app you can imagine</ListItem></Appear>
          </List>
        </Slide>
        <Slide>
          <Heading size={1} fit caps textColor="quaternary">
            Do you know what time it is?
          </Heading>
          <Appear>
            <Heading size={1} fit caps textColor="tertiary">
              Demo time
            </Heading>
          </Appear>
        </Slide>
        <Slide>
          <Heading size={6}><Link href="https://solid.community/">https://solid.community/</Link></Heading>
          <Heading size={6}><Link href="https://angelo.veltens.org/">https://angelo.veltens.org/</Link></Heading>
          <Image src={solidserver}/>
        </Slide>
        <Slide>
          <Heading size={4}><Link href="https://markbook.org/">Markbook</Link></Heading>
          <Image src={markbook}/>
        </Slide>
        <Slide>
          <Heading size={4}><Link href="https://pixolid.netlify.com/">Pixolid</Link></Heading>
          <Image src={pixolid}/>
        </Slide>
        <Slide>
          <Heading>Linked Data</Heading>
          <CodePane textSize={30} source={`
<https://angelo.veltens.org/profile/card#me>
  <http://xmlns.com/foaf/0.1/name> "Angelo Veltens";
  <http://xmlns.com/foaf/0.1/knows>
    <https://ruben.verborgh.org/profile/#me>,
    <https://www.w3.org/People/Berners-Lee/card#i>.
          `} />

        </Slide>
        <Slide>
          <Heading>Short URIs</Heading>
          <CodePane textSize={30} source={`
:me
  <f:name> "Angelo Veltens";
  <f:knows>
    <rubenv:me>,
    <timbl:i>.
          `} />

        </Slide>
        <Slide>
          <Heading>JSON-LD</Heading>
          <CodePane textSize={28} lang="js" source={`
{
  "@context": "/context.jsonld",
  "self": "https://angelo.veltens.org/profile/card#me",
  "name": "Angelo Veltens",
  "knows": [
    "https://ruben.verborgh.org/profile/#me",
    "https://www.w3.org/People/Berners-Lee/card#"
  ]
}
          `} />

        </Slide>

        <Slide bgColor="quaternary" align="flex-start flex-start">
          <div style={{marginTop: '5rem'}}>
            <Heading size={4}>Solid React Components</Heading>
          </div>
          <LiveProvider code={`
     <Value
       src="[https://angelo.veltens.org/profile/card#me].name"
     />
     `}
                        scope={{Value, List: SolidList, Image: SolidImage, Name}}>

            <div style={{backgroundColor: '#111', borderRadius: '5px', caretColor: 'white', width: '100%', marginTop: '1rem',marginBottom: '1rem'}}>
              <LiveEditor/>
            </div>

            <div style={{backgroundColor: 'white', width: '100%',  borderRadius: '5px', padding:'1rem'}}>
              <LiveError/>
              <LivePreview/>
            </div>
          </LiveProvider>
        </Slide>
        <Slide bgColor="quaternary" align="flex-start flex-start">
          <Heading size={6}>Bookmarks</Heading>
          <LiveProvider code={`
     <List
       src="[https://angelo.veltens.org/public/bookmarks].dct_references"
       children={
         (it) =>
         <li>
           <Link href={\`[\${it}][http://www.w3.org/2002/01/bookmark#recalls]\`} >
             <Value src={\`[\${it}].dct_title\`} />
           </Link>
         </li>}
     />
     `}
                        scope={{Value, List: SolidList, Image: SolidImage, Link: SolidLink, Name}}>

            <div style={{display: 'flex', justifyContent: 'space-between'}}>
              <div style={{backgroundColor: '#111', borderRadius: '5px', caretColor: 'white', width: '60%', marginTop: '1rem',marginBottom: '1rem'}}>
                <LiveEditor/>
              </div>

              <div style={{backgroundColor: 'white', width: '100%',  borderRadius: '5px', padding:'1rem',  marginTop: '1rem',marginBottom: '1rem'}}>
                <LiveError/>
                <LivePreview/>
              </div>
            </div>
          </LiveProvider>
        </Slide>
        <Slide>
          <Heading size={1}>Building Solid Apps</Heading>
          <List>
            <ListItem>LDFlex <URL small href="https://github.com/solid/query-ldflex"/></ListItem>
            <ListItem>Solid React Components <URL small href="https://github.com/solid/react-components"/></ListItem>
            <ListItem>Solid React SDK <URL small href="https://github.com/inrupt/solid-react-sdk"/></ListItem>
          </List>
        </Slide>
        <Slide>
          <Heading size={1}>How to contribute?</Heading>
          <List>
            <ListItem>Build Solid Apps</ListItem>
            <ListItem>Build Solid Libraries</ListItem>
            <ListItem>Contribute to Node Solid Server</ListItem>
            <ListItem>Build server modules</ListItem>
            <ListItem>Implement the Solid Specifications</ListItem>
          </List>
        </Slide>
        <Slide>
          <Heading size={1}>Getting started</Heading>
          <List>
            <ListItem><URL href="https://solid.mit.edu" /></ListItem>
            <ListItem><URL href="https://solid.inrupt.net" /></ListItem>
            <ListItem><URL href="https://forum.solidproject.org" /></ListItem>
            <ListItem><URL href="https://gitter.im/solid/" /></ListItem>
          </List>
        </Slide>
        <Slide>
          <Heading caps fit>Questions?</Heading>
          <Heading textColor="quaternary" caps fit>Discussion!</Heading>
        </Slide>
      </Deck>
    );
  }
}

const FriendList =
    <List
        src="[https://angelo.veltens.org/profile/card#me].friend"
        children={(friend) => <li><Value src={`[${friend}].name`}/></li>}
    />;

const FriendFaces =  <List
    src="[https://angelo.veltens.org/profile/card#me].friend"
    children={(friend) => <li><Image src={`[${friend}].image`}/></li>}
/>
