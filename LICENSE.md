Except where otherwise noted, the content of this presentation is licensed under

Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>

https://creativecommons.org/licenses/by-sa/4.0/

Author: [Angelo Veltens](https://angelo.veltens.org/profile/card#me)
